package model.data_structures;
import java.util.Iterator;
public class listaEncadenada<T> implements ILista<T>{
	




		private Nodo<T> primero;
		private Nodo<T> ultimo;
		private int tamano;
		private Nodo<T> posicionActual;
		public listaEncadenada() {
			// TODO Auto-generated constructor stub
			primero=null;
			tamano=0;
			posicionActual=primero;
		}
		
		public Nodo<T> darNodo(int pos) {
			// TODO Auto-generated method stub
			Nodo<T> actual=primero;
			if(primero==null||pos>tamano-1){
				return null;
			}
			for(int i=-1;i<pos-1&&actual!=null;i++){
				actual=actual.darSiguiente();
			}

			return actual;
		}
		
		private class Iterador<T> implements Iterator<T>{
			private int indice;
			public  Iterador() {
				// TODO Auto-generated constructor stub
				indice=-1;
			}
			

			
			public boolean hasNext() {
				// TODO Auto-generated method stub
			return indice+1<tamano;
				
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				if(hasNext()){
					indice++;
					return (T) darElemento(indice);
					}
				
				
				else return null;
				
			}


			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
		}
		public Nodo<T> darPrimero(){
			return primero;
		}
		@Override
		public Iterator<T> iterator() {
			// TODO Auto-generated method stub
			Iterador<T> iterador= new Iterador<T>();
			return iterador;
		}

		@Override
		public void agregarElementoFinal(T elem) {
			// TODO Auto-generated method stub
			Nodo<T> actual=posicionActual;
			Nodo<T> nuevo= new Nodo<>();
			nuevo.modificarItem(elem);
			if(primero ==null){
				primero=nuevo;
				posicionActual=primero;
				tamano++;
				return;
			}
			
			while(actual.darSiguiente()!=null){
				actual=actual.darSiguiente();
			}
			actual.modificarSiguiente(nuevo);
			posicionActual=actual.darSiguiente();
			tamano++;
			
			
			
		}

		@Override
		public T darElemento(int pos) {
			// TODO Auto-generated method stub
			Nodo<T> actual=primero;
			if(primero==null||pos>tamano-1){
				return null;
			}
			for(int i=-1;i<pos-1&&actual!=null;i++){
				actual=actual.darSiguiente();
			}
			
			return actual.darItem();
		}


		@Override
		public int darNumeroElementos() {
			// TODO Auto-generated method stub
			return tamano;
		}

		@Override
		public T darElementoPosicionActual() {
			// TODO Auto-generated method stub
			if(posicionActual==null){
				return null;
			}
			return posicionActual.darItem();
		}

		@Override
		public boolean avanzarSiguientePosicion() {
			// TODO Auto-generated method stub
			if(posicionActual==null||posicionActual.darSiguiente()==null){
				return false;
			}
			posicionActual=posicionActual.darSiguiente();
			return true;
		}

		
		public boolean retrocederPosicionAnterior() {
			// TODO Auto-generated method stub
			if(posicionActual==null||posicionActual.equals(primero)){
				return false;
			}
			Nodo<T> actual=primero;
			Iterador<T> iterador=(Iterador<T>) iterator();
			iterador.next();
			while(!iterador.next().equals(posicionActual.darItem())){
				
				actual=actual.darSiguiente();
			}
			posicionActual=actual;
			
			return true;
		}

	

}
