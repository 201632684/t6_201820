package model.data_structures;

import carga.datos.VOTrip;
import generadorDatos.VOPelicula;


public class heapmax<T extends Comparable<T>> {
	private int capacity;

	private int ultimo;

	private Object S[];
	

	public void crearCP(int i){
		S = new Object[i+1];
		ultimo = 0;
		capacity = i;
		
	}
private int compare(Object x , Object y){
		// no se como compararlos no enteni muy bien la referencia
		return 1;
	}
	
	public int darNumeroDeElementos(){
		return capacity;
	}
	public int tamanoMax(){
		return ultimo;
	}
	public void swap(int i , int j){
		Object temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	}
	public void upHeapBubble(){
		int index = tamanoMax();
		while(index>1){
			int parent = index /2;
			if(compare(S[index], S[parent])>=0){
				break;
			}
			swap(index, parent);
			index = parent;
		}
	}

	public void agregarElemento(VOTrip elemento) throws Exception{
		if(tamanoMax() == capacity){
			throw new Exception("Heap overflow");
		}else{
			ultimo++;
			S[ultimo] = elemento;
			upHeapBubble();
		}

	}
	
}
