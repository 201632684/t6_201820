package model.data_structures;
public class Nodo<T> {
	private T item;
	private Nodo<T> siguiente;
	public T darItem(){
		return item;
	}
	public Nodo<T> darSiguiente(){
		return siguiente;
	}
	public void modificarSiguiente(Nodo<T> pSiguiente){
		siguiente=pSiguiente;
	}
	public void modificarItem(T pItem){
		item=pItem;
	}
}