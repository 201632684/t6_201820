package carga.datos;

import java.time.LocalDateTime;



public class VOTrip implements Comparable<VOTrip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";

    private int tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private int endStationId;
    private String gender;

    public VOTrip(int tripId, LocalDateTime startTime, LocalDateTime stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.gender = gender;
    }

    @Override
    public int compareTo(VOTrip o) {
    	// TODO completar
        return 0;
    }

    public int getTripId() {
        return tripId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
}
